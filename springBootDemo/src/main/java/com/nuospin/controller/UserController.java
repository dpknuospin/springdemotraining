package com.nuospin.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.nuospin.domain.User;
import com.nuospin.dto.LoginDto;
import com.nuospin.exception.UserNotFoundException;
import com.nuospin.service.UserService;
import com.nuospin.util.RestResponse;
import com.nuospin.util.RestUtils;

@RestController
@RequestMapping(value = "user")
public class UserController {

	@Autowired
	private UserService userService;

	@RequestMapping(value = "user/register", method = RequestMethod.POST)
	@ResponseBody
	public User register(@RequestBody @Valid User user){
		return userService.register(user);

	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	@ResponseBody
	public User getUserById(@PathVariable String id ){
		return userService.findById(id);

	}
	
	@RequestMapping(value = "user-email/{email}", method = RequestMethod.GET)
	@ResponseBody
	public User getUserByEmail(@PathVariable String email){
		return userService.findByEmailId(email);

	}
	
	@RequestMapping(value = "/name-password/{name}/{password}", method = RequestMethod.GET)
	@ResponseBody
	public User getUserByNameAndPassword(@PathVariable String name, @PathVariable String password){
		return userService.findByNameAndPassword(name, password);

	}
	@RequestMapping(value = "/login", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<RestResponse<User>> login(@RequestBody @Valid LoginDto loginDto) throws UserNotFoundException{
		return RestUtils.successResponse(userService.login(loginDto));
		
	}
}
