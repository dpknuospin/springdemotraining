package com.nuospin.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.nuospin.domain.User;
import com.nuospin.dto.LoginDto;
import com.nuospin.exception.UserNotFoundException;
import com.nuospin.repositiory.UserRepository;
import com.nuospin.service.UserService;

@Service
public class UserServiceImpl implements UserService {

	@Autowired
	private UserRepository userRepository;

	@Override
	public User register(User user) {
		return userRepository.save(user);

	}

	@Override
	public User findById(String id) {
		return userRepository.findOne(id);
	}

	@Override
	public User findByEmailId(String email) {
		return userRepository.findByEmail(email);
	}

	@Override
	public User findByNameAndPassword(String name, String password) {
		return userRepository.findByNameAndPassword(name, password);
	}

	@Override
	public User login(LoginDto loginDto) throws UserNotFoundException {
		User user = userRepository.findByEmailAndPassword(loginDto.getEmail(), loginDto.getPassword());
		if (user !=null){
			return user;
		}
		throw new UserNotFoundException("Given email id or password are incorrect");
	}
	
	

}
