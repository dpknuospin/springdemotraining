package com.nuospin.service;

import com.nuospin.domain.User;
import com.nuospin.dto.LoginDto;
import com.nuospin.exception.UserNotFoundException;

public interface UserService {
	public User register(User user);
	public User findById(String id);
	public User findByEmailId(String email);
	public User findByNameAndPassword(String name, String password);
	public User login(LoginDto loginDto) throws UserNotFoundException;

}
