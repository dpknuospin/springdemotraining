package com.nuospin.exception;

import org.springframework.http.HttpStatus;
import org.springframework.util.StringUtils;

public class SomethingBadHappenException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5675096637126284089L;

	public static final HttpStatus DEFAULT_HTTP_STATUS = HttpStatus.INTERNAL_SERVER_ERROR;

	public static final String DEFAULT_MESSAGE = "Something Bad Happened !";

	private HttpStatus httpStatus;

	private String message;

	public HttpStatus getHttpStatus() {
		return httpStatus;
	}

	public void setHttpStatus(HttpStatus httpStatus) {
		this.httpStatus = httpStatus;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public SomethingBadHappenException(String message) {
		this(DEFAULT_HTTP_STATUS, message);
	}

	public SomethingBadHappenException() {
		this(DEFAULT_HTTP_STATUS, DEFAULT_MESSAGE);
	}

	public SomethingBadHappenException(HttpStatus httpStatus, String message) {
		super();
		this.httpStatus = httpStatus == null ? DEFAULT_HTTP_STATUS : httpStatus;
		this.message = StringUtils.isEmpty(message) ? DEFAULT_MESSAGE : message;
	}

}
