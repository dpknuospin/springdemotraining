package com.nuospin.exception;

public class UserNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3737009584625061873L;
	
	private static final String DEFAULT_MESAGE = "User not found !";

	public UserNotFoundException() {
		super(DEFAULT_MESAGE);
	}

	public UserNotFoundException(String message) {
		super(message);
	}
	
	

}
