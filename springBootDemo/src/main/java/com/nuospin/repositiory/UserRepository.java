package com.nuospin.repositiory;

import org.springframework.data.mongodb.repository.MongoRepository;

import com.nuospin.domain.User;

public interface UserRepository extends MongoRepository<User, String> {
  public User findByEmail(String email);
  public User findByNameAndPassword(String name,String password);
  public User findByEmailAndPassword(String email, String password);

}
