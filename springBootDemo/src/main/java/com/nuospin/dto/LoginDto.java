package com.nuospin.dto;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.Length;
import org.hibernate.validator.constraints.NotBlank;

public class LoginDto implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8634340578414560243L;
	
	@NotNull
	@NotBlank(message = "Should not be blank ")
	private String email;
	
	@NotNull
	@NotBlank(message = "Should not be blank ")
	@Length(min =4,max =20,message = "Password length should be in between 4 to 20 char")
	private String password;
	
	public LoginDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public LoginDto(String email, String password) {
		super();
		this.email = email;
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

}
